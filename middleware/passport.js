const passport = require("passport");
const jwtStrategy = require("passport-jwt").Strategy;
var cons = require("../cons");
const { ExtractJwt } = require("passport-jwt")
var { userModel } = require("../models/userModel");

passport.use(new jwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken('Authorization'),
    secretOrKey: cons.keyToken
}, async (payload, done) => {
    try {
        console.log('payload', payload);
        const user = await userModel.findOne({ email: payload.sub })
        if (!user) return done(null, false);
        done(null, user)
    } catch (error) {
        done(error, false)
    }
}))
