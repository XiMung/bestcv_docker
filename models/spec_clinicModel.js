var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var spec_clinicSchema = Schema({
    idSpecialize: { type: Schema.ObjectId, ref: "specialize" },
    idClinic: { type: Schema.ObjectId, ref: "clinic" }
});

var spec_clinicModel = mongoose.model("spec_clinic", spec_clinicSchema);
module.exports = {
    spec_clinicModel: spec_clinicModel
}