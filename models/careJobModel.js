var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var careJobSchema = Schema({
    idDoctor: { type: Schema.ObjectId, ref: "doctor" },
    idJob: { type: Schema.ObjectId, ref: "job" }
});

var careJobModel = mongoose.model("careJob", careJobSchema);
module.exports = {
    careJobModel: careJobModel
}