var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var doctorSchema = Schema({
    firstName: String,
    lastName: String,
    idUser: { type: Schema.ObjectId, ref: "user" },
    // idJob: { type: Schema.ObjectId, ref: "job" }
});

var doctorModel = mongoose.model("doctor", doctorSchema);
module.exports = {
    doctorModel: doctorModel
}