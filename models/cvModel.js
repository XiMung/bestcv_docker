var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var cvSchema = Schema({
    avatar: String,
    skill: [String],
    aims: String,
    information: [String],
    address: String
});

var cvModel = mongoose.model("cv", cvSchema);
module.exports = {
    cvModel: cvModel
}