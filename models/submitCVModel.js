var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var submitCVSchema = Schema({
    idTechnical: { type: Schema.ObjectId, ref: "technical" },
    idJob: { type: Schema.ObjectId, ref: "job" },
    isOpen: Boolean,
    isDelete: Boolean,
    isCare: Boolean
});

var submitCVModel = mongoose.model("submitCV", submitCVSchema);
module.exports = {
    submitCVModel: submitCVModel
}