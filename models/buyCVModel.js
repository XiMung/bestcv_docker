var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var buyCVSchema = Schema({
    idClinic: { type: Schema.ObjectId, ref: "clinic" },
    idTechnical: { type: Schema.ObjectId, ref: "technical" }
});

var buyCVModel = mongoose.model("buyCV", buyCVSchema);
module.exports = {
    buyCVModel: buyCVModel
}