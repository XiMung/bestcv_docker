var router = require("express").Router();
var { productModel } = require("../models/productModel");

router.post("/createProduct", createProduct);
router.delete("/deleteProduct", deleteProduct);
router.patch("/updateProduct", updateProduct);
router.get("/getProduct", getProduct);
router.post("/getProductId", getProductId);

module.exports = router;

async function getProductId(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi id"
        })
        return;
    }
    var data = await productModel.findOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function createProduct(req, res) {
    var name = req.body.name;
    if (name == null || name == "" || name == undefined) {
        res.json({
            statusCode: 400,
            message: "loi name"
        })
        return;
    }
    var price = req.body.price;
    if (price == null || price == "" || price == undefined) {
        res.json({
            statusCode: 400,
            message: "loi price"
        })
        return;
    }
    var money = req.body.money;
    if (money == null || money == "" || money == undefined) {
        res.json({
            statusCode: 400,
            message: "loi money"
        })
        return;
    }

    var kind = new productModel({
        name: name, price: price, money: money
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

async function getProduct(req, res) {
    var data = await productModel.find({});
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function updateProduct(req, res) {
    var id = req.body.id;
    var name = req.body.name;
    var price = req.body.price;
    var money = req.body.money;

    let result = await productModel.findOne({ _id: id });

    if (name == null || name == "" || name == undefined) {
        name = result.name;
    }

    if (money == null || money == "" || money == undefined) {
        money = result.money;
    }

    if (price == null || price == "" || price == undefined) {
        price = result.price;
    }

    let data = await productModel.updateOne({ _id: id },
        { name: name, price: price, money: money });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

async function deleteProduct(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await productModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}