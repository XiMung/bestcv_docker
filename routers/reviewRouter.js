var router = require("express").Router();
var { reviewModel } = require("../models/reviewModel");

router.post("/createReview", createReview);
router.delete("/deleteReview", deleteReview);
router.patch("/updateReview", updateReview);
router.get("/getReview", getReview);

module.exports = router;

async function createReview(req, res) {
    var comment = req.body.comment;
    if (comment == null || comment == "" || comment == undefined) {
        res.json({
            statusCode: 400,
            message: "loi comment"
        })
        return;
    }
    var idClinic = req.body.idClinic;
    if (idClinic == null || idClinic == "" || idClinic == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idClinic"
        })
        return;
    }

    var kind = new reviewModel({
        comment: comment,
        idClinic: idClinic
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

async function getReview(req, res) {
    var data = await reviewModel.find({}).populate('idClinic');
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function updateReview(req, res) {
    var id = req.body.id;
    var comment = req.body.comment;

    let result = await reviewModel.findOne({ _id: id });
    if (comment == null || comment == "" || comment == undefined) {
        comment = result.comment;
    }
    let data = await reviewModel.updateOne({ _id: id },
        { comment: comment });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

async function deleteReview(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await reviewModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}