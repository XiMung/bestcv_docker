var router = require("express").Router();
var { buyCVModel } = require("../models/buyCVModel");

router.post("/createBuyCV", createBuyCV);
router.get("/getBuyCV", getBuyCV);
router.post("/getBuyCVId", getBuyCVId);
router.post("/getBuyCVIdClinic", getBuyCVIdClinic);

module.exports = router;

async function getBuyCVIdClinic(req, res) {
    var idClinic = req.body.idClinic;
    if (!idClinic) {
        return res.json({ error: true, message: "id fail" })
    }
    var data = await buyCVModel.find({ idClinic: idClinic })
        .populate({ path: 'idTechnical', populate: { path: 'idDoctor', populate: { path: 'idUser' } } })
        .populate({ path: 'idTechnical', populate: { path: 'idCv' } })
        .populate({ path: 'idTechnical', populate: { path: 'idSpecialize' } })
    if (data) {
        var arr = [];
        data.forEach(a => {
            arr.push(a.idTechnical);
        })
        console.log(arr);
        return res.json({ error: false, data: arr })
    } else {
        return res.json({ error: true, message: "get fail" })
    }
}

async function createBuyCV(req, res) {
    var idClinic = req.body.idClinic;
    if (idClinic == null || idClinic == "" || idClinic == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idClinic"
        })
        return;
    }

    var idTechnical = req.body.idTechnical;
    if (idTechnical == null || idTechnical == "" || idTechnical == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idTechnical"
        })
        return;
    }
    var kind1 = await buyCVModel.findOne({ idClinic: idClinic, idTechnical: idTechnical });
    if (kind1) {
        return res.json({ error: true, message: 'CV đã được mua trước đó !' })
    }

    var kind = new buyCVModel({
        idClinic: idClinic,
        idTechnical: idTechnical
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}
async function getBuyCVId(req, res) {
    var id = req.body.id;
    if (!id) {
        return res.json({ error: true, message: "id fail" })
    }
    var data = await buyCVModel.findOne({ _id: id }).populate({
        path: 'idClinic',
        populate: { path: 'idUser' }
    }).populate({
        path: 'idTechnical', populate: { path: 'idSpecialize' }, populate: { path: 'idDoctor' }, populate: { path: 'idCv' }
    });
    if (data) {
        return res.json({ error: false, data: data })
    } else {
        return res.json({ error: true, message: "get fail" })
    }
}

async function getBuyCV(req, res) {
    var data = await buyCVModel.find().populate({
        path: 'idClinic',
        populate: { path: 'idUser' }
    }).populate({
        path: 'idTechnical',
        populate: { path: 'idSpecialize' },
        populate: { path: 'idDoctor' },
        populate: { path: 'idCv' }
    }).sort({ _id: -1 });
    if (data) {
        return res.json({ error: false, data: data })
    } else {
        return res.json({ error: true, message: "get fail" })
    }
}