var router = require("express").Router();
var { spec_clinicModel } = require("../models/spec_clinicModel");

router.post("/createSpec_clinic", createSpec_clinic);
router.delete("/deleteSpec_clinic", deleteSpec_clinic);
router.patch("/updateSpec_clinic", updateSpec_clinic);
router.get("/getSpec_clinic", getSpec_clinic);

module.exports = router;

async function createSpec_clinic(req, res) {
    var idSpecialize = req.body.idSpecialize;
    if (idSpecialize == null || idSpecialize == "" || idSpecialize == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idSpecialize"
        })
        return;
    }
    var idClinic = req.body.idClinic;
    if (idClinic == null || idClinic == "" || idClinic == undefined) {
        res.json({
            statusCode: 400,
            message: "loi idClinic"
        })
        return;
    }

    var kind = new spec_clinicModel({
        idSpecialize: idSpecialize, idClinic: idClinic
    });
    var saveKind = await kind.save();
    if (saveKind) {
        return res.json({ error: false, data: saveKind });
    }
    else {
        return res.json({ error: true, message: 'Them that bai' });
    }
}

async function getSpec_clinic(req, res) {
    var data = await spec_clinicModel.find({}).populate('idSpecialize').populate('idClinic');
    if (data) {
        return res.json({ error: false, data: data });
    }
    else {
        return res.json({ error: true, message: 'hehe' });
    }
}


async function updateSpec_clinic(req, res) {
    var id = req.file;
    var idSpecialize = req.body.avatar;
    var idClinic = req.body.skill;

    let result = await spec_clinicModel.findOne({ _id: id });

    if (idSpecialize == null || idSpecialize == "" || idSpecialize == undefined) {
        idSpecialize = result.idSpecialize;
    }

    if (idClinic == null || idClinic == "" || idClinic == undefined) {
        idClinic = result.idClinic;
    }

    let data = await spec_clinicModel.updateOne({ _id: id },
        { idSpecialize: idSpecialize, idClinic: idClinic });
    if (data) {
        return res.json({ err: false, data: data })
    } else {
        return res.json({ err: true, message: "loi update" })
    }
}

async function deleteSpec_clinic(req, res) {
    var id = req.body.id;
    if (id == null || id == "" || id == undefined) {
        res.json({
            statusCode: 400,
            message: "loi xoa"
        })
        return;
    }
    let data = await spec_clinicModel.deleteOne({ _id: id });
    if (data) {
        return res.json({ error: false, data: data });
    } else {
        return res.json({ error: true, message: 'loi xoa' })
    }
}