### STAGE 1: Build ###
FROM node:10.22.1
RUN mkdir /usr/app
COPY package.json ./usr/app
WORKDIR /usr/app
RUN npm install --quiet
COPY . .

# production environment
EXPOSE 1998
CMD ["npm", "start"]

